
const validateTextInput = function(inputEl, submitEvent) {

    const errorEl = inputEl.parentElement.querySelector('.error');
    if (inputEl.value === '' || inputEl.value.length < 3 ){

        const labelEl = inputEl.parentElement.querySelector('label');
        errorEl.innerHTML = `${labelEl.innerText} is required and must be 3 characters or more`;
        inputEl.parentElement.classList.add('invalid');

        submitEvent.preventDefault();
        console.log('Bad input');
        
    } else {
        inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = '';
    }

  };
  const validateEmailInput = function(inputEl, submitEvent) {

    const errorEl = inputEl.parentElement.querySelector('.error');
    var reg = /\w+@\w+\.\w+/;

    if (reg.test(inputEl.value) === false) 
        {
            errorEl.innerHTML = `Email address isn't in proper format.`;
            inputEl.parentElement.classList.add('invalid');
            submitEvent.preventDefault();
        }else {
         inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = '';
    } 
  };

  const fName = document.getElementsByTagName('input')[0];
  const lName = document.getElementsByTagName('input')[1];
  const emailEl = document.getElementsByTagName('input')[2];
  

  const formEl = document.getElementById('connect-form')
      .addEventListener('submit', function(e) {
        // for (let i = 0; i < inputElements.length; i++) {
        validateTextInput(fName, e);
        validateTextInput(lName, e);
        validateEmailInput(emailEl, e);
        //e.preventDefault();
      });   

 
   