let year = document.getElementsByName("year")[0];
let month = document.getElementsByName("month")[0];
let day = document.getElementsByName("date")[0];


document.getElementById("submit").addEventListener("click", function(e) {

  e.preventDefault();

var Year = year.value;
var Month = month.value;
var Day = day.value;
// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists/';

const url = `${BASE_URL}/${Year}-${Month}-${Day}/hardcover-fiction.json?q=best selling book&api-key=${API_KEY}`;

fetch(url)
  .then(function(response) {
    return response.json(); //Converts to a JavaScript Object
  })
  .then(function(responseJson){
    console.log(responseJson);

    let book1 = responseJson.results.books[0];
    let book2 = responseJson.results.books[1];
    let book3 = responseJson.results.books[2];
    let book4 = responseJson.results.books[3];
    let book5 = responseJson.results.books[4];
    console.log(book1);
    const imgUrl1 = book1.book_image;
    const  title1 = book1.title; 
    const  author1 = book1.author;
    const  description1 = book1.description;

    const imgUrl2 = book2.book_image;   
    const  title2 = book2.title; 
    const  author2 = book2.author;
    const  description2 = book2.description;
    
    const imgUrl3 = book3.book_image;
    const  title3 = book3.title; 
    const  author3 = book3.author;
    const  description3 = book3.description;
   
    const imgUrl4 = book4.book_image;
    const  title4 = book4.title; 
    const  author4 = book4.author;
    const  description4 = book4.description;
    
    const imgUrl5 = book5.book_image;
    const  title5 = book5.title; 
    const  author5 = book5.author;
    const  description5 = book5.description;

    document.getElementById('books-container').innerHTML = `
    <h3>Top Seller</h3><h4>${title1}</h4><img src="${imgUrl1}" height="300px"></img><h5>by ${author1}</h5><p>Description</p><p>${description1}</p>
    <h4>${title2}</h4><img src="${imgUrl2}" height="300px"></img><h5>by ${author2}</h5><p>Description</p><p>${description2}</p>
    <h4>${title3}</h4><img src="${imgUrl3}" height="300px"></img><h5>by ${author3}</h5><p>Description</p><p>${description3}</p>
    <h4>${title4}</h4><img src="${imgUrl4}" height="300px"></img><h5>by ${author4}</h5><p>Description</p><p>${description4}</p>
    <h4>${title5}</h4><img src="${imgUrl5}" height="300px"></img><h5>by ${author5}</h5><p>Description</p><p>${description5}</p>
    `;
    
  })
});