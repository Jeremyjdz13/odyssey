/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl 
 * @param {Event} submitEvent 
 */
const validateItem = function(inputEl, submitEvent) {
  const errorEl = inputEl.parentElement.querySelector('.error');

  if (inputEl.value === '' || inputEl.value.length < 3 ) {
    const labelEl = inputEl.parentElement.querySelector('label');
    if (!inputEl.value) {
      errorEl.innerHTML = `${labelEl.innerText} is Required`;
    } else {
      errorEl.innerHTML = `${labelEl.innerText} must be more than 3 characters`;
    }
    errorEl.classList.add('d-block');

    // Prevent form submit
    submitEvent.preventDefault();

  } else {
    errorEl.innerHTML = '';
    errorEl.classList.remove('d-block');
  }
}

const validateEmailInput = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');
    var reg = /\w+@\w+\.\w+/;
    
      if (reg.test(inputEl.value) === false) 
        {
            const labelEl = inputEl.parentElement.querySelector('label');
            if (!inputEl.value) {
              errorEl.innerHTML = `${labelEl.innerText} is Required`;
            } else {
              errorEl.innerHTML = `${labelEl.innerText} is not in the proper format`;
            }
            errorEl.classList.add('d-block');
            // Prevent form submit
            submitEvent.preventDefault();
        
          } else {
            errorEl.innerHTML = '';
            errorEl.classList.remove('d-block');
          }
      };

      const validateMessageInput = function(inputEl, submitEvent) {
        const errorEl = inputEl.parentElement.querySelector('.error');
          if (inputEl.value.length < 10) 
            {
                const labelEl = inputEl.parentElement.querySelector('label');
                if (!inputEl.value) {
                  errorEl.innerHTML = `Message is Required`;
                } else {
                  errorEl.innerHTML = `Please write more than 10 characters`;
                }
                errorEl.classList.add('d-block');
                // Prevent form submit
                submitEvent.preventDefault();
            
              } else {
                errorEl.innerHTML = '';
                errorEl.classList.remove('d-block');
              }
          };

          const validateURLInput = function(inputEl, submitEvent) {
            const errorEl = inputEl.parentElement.querySelector('.error');
            var reg = /https?\:\/\/.+\..+/
              if (reg.test(inputEl.value) === false) 
                {
                    const labelEl = inputEl.parentElement.querySelector('label');
                    if (!inputEl.value) {
                      errorEl.innerHTML = `URL is Required`;
                    } else {
                      errorEl.innerHTML = `Your web address requires https: format`;
                    }
                    errorEl.classList.add('d-block');
                    // Prevent form submit
                    submitEvent.preventDefault();
                
                  } else {
                    errorEl.innerHTML = '';
                    errorEl.classList.remove('d-block');
                  }
              };
                

const inputElements = document.getElementsByClassName('validate-input');

const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
      for (let i = 0; i < inputElements.length; i++) {
        validateItem(inputElements[i], e);
        validateEmailInput(inputElements[2],e);
        validateMessageInput(inputElements[3],e);
        validateURLInput(inputElements[5],e);
      }

    e.preventDefault();
    });

    // fucntion codeValidation(){
    // var codeSelect = document.getElementById('codeSelection');
    // var inputVal = codeSelect[codeSelect.selectedIndex].value
    //   if(inputVal == 0){
    //     errorEl.innerHTML =  `You must select a code Language`;
    //   }else {
    //     errorEl.innerHTML = '';
    //   }
    //   submitEvent.preventDefault();
    //  };        