
/*D100 Section Code*/
//Select Column
const hSelect = document.getElementsByClassName('hselect')[0];

//Dice 10 and 100 roll event listeners
const HD10 = document.getElementById('HD10');

HD10.addEventListener("click", function(e){
  e.preventDefault();
  var baseRank = document.getElementById('initDefault').value;
  let rTimer = window.setInterval(function(){
  let roll =  Math.floor((Math.random() * 10) +1);
  let total = parseInt(baseRank) + parseInt(roll);
  document.getElementById('h10Results').innerHTML = `${baseRank} + ${roll} = ${total}` ;
  document.getElementById('HD10').disabled = true;
  },100);  
  window.setTimeout(()=> {
   clearInterval(rTimer); 
   document.getElementById('HD10').disabled = false;
  },2000);
});

//Modifiers D100
var counter = 0;
document.getElementById('displayModifier').innerHTML = counter;
const plusEl = document.getElementById('plus');
plusEl.addEventListener('click', function(e) {
  counter+= 5;
  document.getElementById('displayModifier').innerHTML = counter;
});

const minusEl = document.getElementById('minus');
minusEl.addEventListener('click', function(e) {
  counter-= 5;
  document.getElementById('displayModifier').innerHTML = counter;
});
//End of Modifiers D100
// d100ModifierEl.addEventListener('input',function(){ 
// });
// let d100Modifier = d100ModifierEl;

const HD100 = document.getElementById('HD100');
HD100.addEventListener("click", function(e){
    e.preventDefault();
    var powerRank = document.getElementById('powerRank');
    var d100ModifierEl = document.getElementById('displayModifier').value;

    let rTimer = window.setInterval(function(){
    let roll =  Math.floor((Math.random() * 100) +1);
    let d100Total = parseInt(d100ModifierEl) + parseInt(roll);
    document.getElementById('h100Results').innerHTML = `${d100ModifierEl} + ${roll} = ${d100Total}`;
    document.getElementById('HD100').disabled = true;
    if (powerRank.value == '1'){ //Decrepit
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 55){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 56 && d100Total <= 94){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 95 && d100Total <= 99){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 100){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '2'){//Feebel 
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 50){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 51 && d100Total <= 90){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 91 && d100Total <= 99){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 100){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '3'){//poor
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 45){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 46 && d100Total <= 85){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 86 && d100Total <= 99){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 100){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '4'){//Typical
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 40){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 41 && d100Total <= 80){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 81 && d100Total <= 97){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 98){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '5'){//Good
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 35){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 36 && d100Total <= 75){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 76 && d100Total <= 97){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 98){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '6'){//Excellent
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 30){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 31 && d100Total <= 70){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 71 && d100Total <= 94){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 95){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '7'){//Remarkable
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 25){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 26 && d100Total <= 65){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 66 && d100Total <= 94){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 95){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '8'){//Incredible
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 20){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 21 && d100Total <= 60){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 61 && d100Total <= 90){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 91){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '9'){//Amazing
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 15){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 16 && d100Total <= 55){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 56 && d100Total <= 90){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 91){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '10'){//Monstrous
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 10){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 11 && d100Total <= 50){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 51 && d100Total <= 85){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 86){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } else if (powerRank.value == '11'){//Unearthly
      if (d100Total <= 1){
        document.getElementsByName('Success Results')[0].innerHTML = "Oh boy... you Botched!"
      }else if(d100Total >= 2 && d100Total <= 6){
        document.getElementsByName('Success Results')[0].innerHTML = "Failed!"
      }else if (d100Total >= 7 && d100Total <= 45){
        document.getElementsByName('Success Results')[0].innerHTML = "Green Success!"
      }else if (d100Total >= 46 && d100Total <= 85){
        document.getElementsByName('Success Results')[0].innerHTML = "Yellow Success!"
      }else if (d100Total >= 86){
        document.getElementsByName('Success Results')[0].innerHTML = "Red Success!"
     } 
    } 
  },100);
    window.setTimeout(()=> {
      clearInterval(rTimer);
      document.getElementById('HD100').disabled = false;
      const successEl = document.getElementsByClassName('successArea')[0];
      let successVal = successEl.value;
      console.log(successVal);
      if (successVal === "Red Success!"){
      const Success_URL =`https://api.giphy.com/v1/stickers/random?api_key=${API_KEY}&tag=success&rating=R`;
      fetch(Success_URL)
        .then(function(response){
          return response.json();
         })
          .then(function(responseJson){
          // console.log(responseJson);
          let sImage = responseJson.data.fixed_height_downsampled_url;
          document.getElementById('randomImage').innerHTML = `<img id="rImage" src="${sImage}"></img>`;
          setTimeout(function(){
            let RImage = document.getElementById('rImage');
            console.log(RImage);
            document.querySelector('img').remove();
          }, 10000);
        });
      }else if (successVal === "Oh boy... you Botched!"){
      const Failed_URL =`https://api.giphy.com/v1/stickers/random?api_key=${API_KEY}&tag=failed&rating=R`;
      fetch(Failed_URL)
        .then(function(response){
          return response.json();
         })
          .then(function(responseJson){
          // console.log(responseJson);
          let fImage = responseJson.data.fixed_height_downsampled_url;
          document.getElementById('randomImage').innerHTML = `<img id="rImage" src="${fImage}"></img>`;
          setTimeout(function(){
            let RImage = document.getElementById('sImage');
            console.log(RImage);
            document.querySelector('img').remove();
          }, 10000);
        });
      }
    },2000);
});
    
// const powerRank = document.querySelector('select');
//     powerRank.addEventListener('change', function(e){  
//   });  function for highlighting chart elements.

//End of Dice 10 and 100 roll event listener code

const addPlayer = document.getElementById('add');
addPlayer.addEventListener('click',function(){
  let playerName = document.getElementById('heroName').value;
  let playerInitiative = document.getElementById('initDefault').value;
  var div = document.createElement('div')
  div.className = "pList"
  div.innerHTML = `<h3>${playerName}</h3><h5>Base Initiative ${playerInitiative}</h5><input class="remove" type="button" value="X"></input>`
  document.getElementById('playerList').appendChild(div);
 
  var btn = document.getElementsByClassName('remove')
 
  for (var i = 0; i < btn.length; i++) {
    btn[i].addEventListener('click', function(e) {
      this.parentNode.remove();
    }, false);
  }
 });


const load = document.getElementById('getPlayerList');
load.addEventListener('click',function(){
  document.getElementById("playerList").innerHTML = localStorage.getItem("PlayerList");
})



const save = document.getElementById('savePlayerList');
save.addEventListener('click',function(){
  var items = document.getElementById('playerList').innerHTML;
  localStorage.setItem("PlayerList", items);

})


document.cookie