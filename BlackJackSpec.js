// Kept the starter Jasmine Player Spec Code
// Update this!

describe("dealerShouldDraw", function() {

  it("should be able to calculate Black Jack hand", function() {
    const hand1 = [{
      suit: 'diamonds',
      val: 9,
      displayVal: "9"
    },
    {
      suit: 'hearts',
      val: 10,
      displayVal: '10',
    }];
    const hand2 = [{
      suit: 'spades',
      val: 6,
      displayVal: '6'
    },
    {
      suit: 'spades',
      val: 1,
      displayVal: 'Ace',
    }];
    const hand3 = [{
      suit: 'clubs',
      val: 10,
      displayVal: '10'
    },
    {
      suit: 'clubs',
      val: 7,
      displayVal: '7'
    }];
    const hand4 = [{
      suit: 'clubs',
      val: 2,
      displayVal: '2'
    },
    {
      suit: 'clubs',
      val: 4,
      displayVal: '4'
    },
    {
      suit: 'hearts',
      val: 2,
      displayVal: '2'
    },
    {
      suit: 'spades',
      val: 5,
      displayVal: '5'
    }];
    expect(calcPoints(hand1).total).toBe(19);
    expect(calcPoints(hand2).total).toBe(17);
    expect(calcPoints(hand3).total).toBe(17);
    expect(calcPoints(hand4).total).toBe(13);
    
  });
  
  describe("dealerShouldDraw", function(){
    const hand1 = [{
      suit: 'diamonds',
      val: 9,
      displayVal: "9"
    },
    {
      suit: 'hearts',
      val: 10,
      displayVal: '10',
    }];
    const hand2 = [{
      suit: 'spades',
      val: 6,
      displayVal: '6'
    },
    {
      suit: 'spades',
      val: 1,
      displayVal: 'Ace',
    }];
    const hand3 = [{
      suit: 'clubs',
      val: 10,
      displayVal: '10'
    },
    {
      suit: 'clubs',
      val: 7,
      displayVal: '7'
    }];
    const hand4 = [{
      suit: 'clubs',
      val: 2,
      displayVal: '2'
    },
    {
      suit: 'clubs',
      val: 4,
      displayVal: '4'
    },
    {
      suit: 'hearts',
      val: 2,
      displayVal: '2'
    },
    {
      suit: 'spades',
      val: 5,
      displayVal: '5'
    }];
    // var DealerHand = (calcPoints(hand1).total);
    it("Should determine if dealer should draw", function(){
      expect(dealerShouldDraw(hand1)).toBe(false);
      expect(dealerShouldDraw(hand2)).toBe(true);
      expect(dealerShouldDraw(hand3)).toBe(false);
      expect(dealerShouldDraw(hand4)).toBe(true);
      
    })
    })
});



