// Kept the starter Jasmine Player Spec Code
// Update this!

describe("calcPoints", function() {

  it("should be able to calculate Black Jack hand", function() {
    const hand1 = [{
      suit: 'diamonds',
      val: 7,
      displayVal: "7"
    },
    {
      suit: 'hearts',
      val: 10,
      displayVal: '10',
    }];
    const hand2 = [{
      suit: 'spades',
      val: 9,
      displayVal: '9'
    },
    {
      suit: 'spades',
      val: 1,
      displayVal: 'Ace',
    }];
    const hand3 = [{
      suit: 'clubs',
      val: 10,
      displayVal: '10'
    },
    {
      suit: 'clubs',
      val: 6,
      displayVal: '6'
    },
    {
      suit: 'diamonds',
      val: 1,
      displayVal: 'Ace' 
    }];
    expect(calcPoints(hand1).total).toBe(17);
    expect(calcPoints(hand2).total).toBe(20);
    expect(calcPoints(hand3).total).toBe(17);
  });
});



//   describe("when song has been paused", function() {
//     beforeEach(function() {
//       player.play(song);
//       player.pause();
//     });

//     it("should indicate that the song is currently paused", function() {
//       expect(player.isPlaying).toBeFalsy();

//       // demonstrates use of 'not' with a custom matcher
//       expect(player).not.toBePlaying(song);
//     });

//     it("should be possible to resume", function() {
//       player.resume();
//       expect(player.isPlaying).toBeTruthy();
//       expect(player.currentlyPlayingSong).toEqual(song);
//     });
//   });

//   // demonstrates use of spies to intercept and test method calls
//   it("tells the current song if the user has made it a favorite", function() {
//     spyOn(song, 'persistFavoriteStatus');

//     player.play(song);
//     player.makeFavorite();

//     expect(song.persistFavoriteStatus).toHaveBeenCalledWith(true);
//   });

//   //demonstrates use of expected exceptions
//   describe("#resume", function() {
//     it("should throw an exception if song is already playing", function() {
//       player.play(song);

//       expect(function() {
//         player.resume();
//       }).toThrowError("song is already playing");
//     });
//   });
// });
