

let number = 1;

const lightenBackGround = function(){
    if (number >=255){return; }
    const backGroundColor = `rgb(${number},${number},${number})`;
    document.getElementById('bg').style.backgroundColor = backGroundColor;
    number++;
    requestAnimationFrame(lightenBackGround);
}

requestAnimationFrame(lightenBackGround);

let text = 255;

const darkenText = function(){
    if (text <= 0){return;}
    const textColor = `rgb(${text},${text},${text})`;
    document.getElementsByTagName('h1')[0].style.color = textColor;
    text --;
    requestAnimationFrame(darkenText);
}

requestAnimationFrame(darkenText);