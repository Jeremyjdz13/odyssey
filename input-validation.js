

const validateHeroName = function(inputEl, event) {
  const errorEl = inputEl.parentElement.querySelector('.error');
  if (!/\w/.test(inputEl.value)) {
      errorEl.innerHTML = `Please enter a name so we can store your info and sell it on the black market.  Thanks`;
      errorEl.classList.add('d-block');
      // Prevent form submit
      event.stopImmediatePropagation();
    } else {
      errorEl.innerHTML = '';
      errorEl.classList.remove('d-block');
    }
}

const validateInitiative = function(inputEl, event) {
  const errorEl = inputEl.parentElement.querySelector('.error');
    // var reg = /^([1-9]|10|11)$/;
    if (/\D/.test(inputEl.value) || inputEl.value === '' ||  inputEl.value < 0 || inputEl.value > 11 ) {
      if (!inputEl.value) {
        errorEl.innerHTML = `You must add a default value`;
      } else {
        errorEl.innerHTML = `You must enter a number from 0 to 11`;
      } 
      errorEl.classList.add('d-block');
      // Prevent form submit
      event.stopImmediatePropagation();
    } else {
      errorEl.innerHTML = '';
      errorEl.classList.remove('d-block');
    }
  }
const validateSelection = function(inputEl, event) {
  const errorEl = inputEl.parentElement.querySelector('.error');
  if (inputEl.value == 0) {
      errorEl.innerHTML = `You must select a Power Rank!`;
      errorEl.classList.add('d-block');
      // Prevent form submit
      event.stopImmediatePropagation();
    } else {
      errorEl.innerHTML = '';
      errorEl.classList.remove('d-block');
    }
}
const inputElements = document.getElementsByClassName('validate-input');

const heroName = document.getElementById('add')
    .addEventListener('click', function(e) {
    for (let i = 0; i < inputElements.length; i++){
    validateHeroName(inputElements[0],e);
    }
    e.preventDefault();
});

const initRoll = document.getElementById('HD10')
    .addEventListener('click', function(e) {
    for (let i = 0; i < inputElements.length; i++){
    validateInitiative(inputElements[1],e);
    }
    e.preventDefault();
});

const powerRoll = document.getElementById('HD100')
    .addEventListener('click', function(e) {
    for (let i = 0; i < inputElements.length; i++){
    validateSelection(inputElements[2],e);
    }
    e.preventDefault();
});

