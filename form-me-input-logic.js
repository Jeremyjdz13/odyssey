const formSelect = document.querySelector('select');
const businessEls = document.getElementsByClassName('business');
const technicalEls = document.getElementsByClassName('technical');

const removeClassFromCollection = function(collection, className) {
  for(let i = 0; i < collection.length; i++) {
    collection[i].classList.remove(className)
  }
}

const addClassToCollection = function(collection, className) {
  for(let i = 0; i < collection.length; i++) {
    collection[i].classList.add(className)
  }
}

formSelect.addEventListener('change', function(e) {
  if (this.value === 'business') {
    removeClassFromCollection(businessEls, 'd-none');
    addClassToCollection(technicalEls, 'd-none');

  } else {
    removeClassFromCollection(technicalEls, 'd-none');
    addClassToCollection(businessEls, 'd-none');
  }

});

// Local Storage

const storageBtn = document.getElementById('store');
// event listener
storageBtn.addEventListener("click",myFucntion);

 function myFucntion(){
   //colect Job Title and company URL
  var title = document.getElementById('jobTitle').value;
  var coURL = document.getElementById('coUrl').value;
  var jobInfo = [title, coURL];
  //pass data to local storage
   localStorage.setItem('Business', JSON.stringify(jobInfo));
  
  localStorage.getItem('jobInfo');

};

const clearBtn = document.getElementById('clear');

clearBtn.addEventListener("click",myClearFunction);

function myClearFunction(){
  localStorage.clear('jobInfo');
}






